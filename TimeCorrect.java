import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

public class TimeCorrect {
	private static Date returnDate() throws Exception {
		String TIME_SERVER = "pool.ntp.org";
		NTPUDPClient  Client = new NTPUDPClient();	
		TimeInfo timeInfo = null;
		try {
		InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
		 timeInfo = Client.getTime(inetAddress);
		}catch(Exception e) {
			System.out.println("Network error");
		}
		long returnTime = timeInfo.getReturnTime();
		
		Date dateTime = new Date(returnTime);
		return dateTime;
	}
	
	
	public static void main(String[] args) {
		String patternDate = "dd-MM-yyyy ";
		String patternTime = "HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(patternDate);
		SimpleDateFormat tsdf = new SimpleDateFormat(patternTime);
		
			try {
				Date d = returnDate();
				String date = sdf.format(d);
				String time = tsdf.format(d);
				System.out.println("Current date: " + date);
				System.out.println("Current time: " + time);
			} catch (Exception e) {	 
				e.printStackTrace();
			}
		
		
	}

}
